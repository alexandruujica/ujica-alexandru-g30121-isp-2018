package ujica.alexandru.lab9.ex3;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.FileReader;

public class FileContent extends JFrame {
    JTextField l1;
    JButton b1;
    JTextArea t1;


    FileContent() {
        setTitle("Casuta");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(400, 500);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        int width = 300;
        int height = 40;

        l1 = new JTextField();
        l1.setBounds(10, 50, width, height);

        b1 = new JButton("Click");
        b1.setBounds(10, 110, 70, height);
        b1.addActionListener(new tratareButon());

        t1 = new JTextArea();
        t1.setBounds(10, 180, width, 300);

        add(l1);
        add(b1);
        add(t1);
    }

    public static void main(String[] args) {
        FileContent a = new FileContent();
    }

    class tratareButon implements ActionListener{
        public void actionPerformed(ActionEvent e){
            String s1=FileContent.this.l1.getText();
            String s2=new String();
            try{
                BufferedReader b=new BufferedReader(new FileReader(s1));
                s2=b.readLine();
            }catch (Exception f){
                FileContent.this.t1.append("not found");
                throw new RuntimeException(f);
            }
            FileContent.this.t1.append(s2);
        }
    }
}
