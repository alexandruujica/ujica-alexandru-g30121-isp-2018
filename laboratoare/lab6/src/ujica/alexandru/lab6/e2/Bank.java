package ujica.alexandru.lab6.e2;

import ujica.alexandru.lab6.e2.BankAccount;

import java.util.ArrayList;

public class Bank {

    ArrayList<BankAccount> myList = new ArrayList<>();

    public void addAccount(String owner, double balance) {
        BankAccount b1 = new BankAccount(owner, balance);
        myList.add(b1);
    }

    public void printAccounts() {
        boolean a;
        do {
            a = false;
            for (int i = 0; i < myList.size() - 1; i++) {
                BankAccount b1 = (BankAccount) myList.get(i);
                BankAccount b2 = (BankAccount) myList.get(i + 1);
                if (b1.getBalance() > b2.getBalance()) {
                    myList.set(i, b2);
                    myList.set(i + 1, b1);
                    a = true;
                }
            }

        } while (a);

        for (BankAccount b : myList) {
            System.out.println(b.getOwner() + "   value    " + b.getBalance());
        }
    }

    public void printAccounts(double minBalance, double maxBalance) {
        for (BankAccount b : myList) {
            if ((b.getBalance() >= minBalance) && (b.getBalance() <= maxBalance)) {
                System.out.println(b.getOwner() + "  Value:  " + b.getBalance());
            }
        }
    }

    BankAccount ba = new BankAccount("", 0);

    public BankAccount getAccount(String owner) {
        for (int i = 0; i < myList.size() - 1; i++) {
            BankAccount b = (BankAccount) myList.get(i);
            if (b.getOwner().equals(owner)) {
                ba=myList.get(i);
            }

        }
        return ba;
    }

    public void getAllAccounts() {
        boolean a;
        do {
            a = false;
            for (int i = 0; i < myList.size() - 1; i++) {
                BankAccount b1 = (BankAccount) myList.get(i);
                BankAccount b2 = (BankAccount) myList.get(i + 1);
                int j;
                j = b1.getOwner().compareTo(b2.getOwner());
                if (j > 0) {
                    myList.set(i, b2);
                    myList.set(i + 1, b1);
                    a = true;
                }
            }
        } while (a);
        for (BankAccount b : myList) {
            System.out.println(b.getOwner() + "   value    " + b.getBalance());
        }
    }
}
