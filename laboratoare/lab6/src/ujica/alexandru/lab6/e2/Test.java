package ujica.alexandru.lab6.e2;

public class Test {
    public static void main(String[] args) {
        Bank b1=new Bank();
        b1.addAccount("Mircea",20.0);
        b1.addAccount("Vlad",35.0);
        b1.addAccount("Sergiu",86.0);
        b1.addAccount("Mircea",76.0);
        b1.addAccount("Ionel",74.0);
        b1.addAccount("Flavius",22.0);
        System.out.println("all acounts: ");
        b1.printAccounts();
        System.out.println();
        b1.printAccounts(70.0,88.0);
        System.out.println();
        b1.getAllAccounts();
    }

}
