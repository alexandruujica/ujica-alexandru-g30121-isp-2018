package ujica.alexandru.lab6.e4;


import java.util.Scanner;

public class ConsoleMenu {
    public static void main(String[] args) {
        Word[] a = new Word[5];
        Definition[] b = new Definition[5];
        Dictionary d = new Dictionary();

        a[0] = new Word("manca");
        b[0] = new Definition("Gest");
        d.addWord(a[0], b[0]);
        a[1] = new Word("pateu");
        b[1] = new Definition("mancare");
        d.addWord(a[1], b[1]);
        a[2] = new Word("cina");
        b[2] = new Definition("masa");
        d.addWord(a[2], b[2]);
        a[3] = new Word("pita");
        b[3] = new Definition("aliment");
        d.addWord(a[3], b[3]);
        a[4] = new Word("nume");
        b[4] = new Definition("descriere");
        d.addWord(a[4], b[4]);
        Boolean h = true;
        while (h == true) {
            Scanner scan = new Scanner(System.in);
            System.out.println("1.Add element");
            System.out.println("2.Get definition for the world");
            System.out.println("3.Print all");
            System.out.println("4.Print definitions");
            System.out.println("5.Print evrything");
            int i = scan.nextInt();
            switch (i) {
                case 1: {
                    System.out.println("Give the world");
                    Scanner sc=new Scanner(System.in);
                    String j=sc.nextLine();
                    Word wr=new Word(j);
                    System.out.println("The definition:");
                    Scanner sd=new Scanner(System.in);
                    String w=sd.nextLine();
                    Definition def=new Definition(w);
                    d.addWord(wr,def);
                    break;
                }
                case 2: {
                    System.out.println("The word: ");
                    Scanner sd=new Scanner(System.in);
                    String j=sd.nextLine();
                    System.out.println(j);
                    Word wr=new Word(j);
                    System.out.println("The definition: "+d.getDefinition(wr).getDescription());
                    break;
                }
                case 3: {
                    d.allset();
                    System.out.println();
                    break;
                }
                case 4: {
                    d.getAllWords();
                    System.out.println();
                    break;
                }
                case 5: {
                    d.getAllDefinitions();
                    break;
                }
                default:
                    h = false;
            }
        }
    }
}

