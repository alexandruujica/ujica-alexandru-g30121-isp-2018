package ujica.alexandru.lab6.e4;

import java.util.*;

public class Dictionary {
    HashMap<Word,Definition>dictionary=new HashMap<Word,Definition>();


    public void addWord(Word w, Definition d) {
        dictionary.put(w, d);
    }


    public Definition getDefinition(Word w) {
        Definition def;
        for(Word s:dictionary.keySet()){
            if (s.getName().equals(w.getName())){
                def=dictionary.get(s);
                return def;
            }
        }
        String j="nu exista cuvantul in dictionar";
        def=new Definition(j);
        return def;

    }

    public void allset() {
        for (Word s : dictionary.keySet()) {
            System.out.println(s.getName() + "    " + dictionary.get(s).getDescription());

        }
    }

    public void getAllWords() {
        for (Word s : dictionary.keySet()) {
            System.out.println(s.getName());
        }
    }

    public void getAllDefinitions() {
        for (Word s : dictionary.keySet()) {
            System.out.println(dictionary.get(s).getDescription());
        }

    }

}
