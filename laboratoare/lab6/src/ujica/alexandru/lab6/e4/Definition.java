package ujica.alexandru.lab6.e4;

public class Definition {
    private String description;

    public Definition(String descrp){
        this.description=descrp;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
