package ujica.alexandru.lab6.e1;

import java.util.Objects;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance) {
        this.owner=owner;
        this.balance=balance;
    }

    public void withdraw(double amount) {
        this.balance -= amount;
    }

    public void deposit(double amount) {
        this.balance -= amount;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }


    public boolean equals(BankAccount o) {
        if (this == o) return true;
        return Double.compare(this.balance, o.balance) == 0 &&
                this.owner.equals(o.owner);
    }
}
