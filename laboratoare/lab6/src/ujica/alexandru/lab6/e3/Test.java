package ujica.alexandru.lab6.e3;

public class Test {
    public static void main(String[] args) {
        Bank bank=new Bank();
        bank.addAccount("Mircea",20.0);
        bank.addAccount("Vlad",35.0);
        bank.addAccount("Sergiu",86.0);
        bank.addAccount("Mircea",76.0);
        bank.addAccount("Ionel",74.0);
        bank.addAccount("Flavius",22.0);
        System.out.println("all acounts: ");
        bank.printAccounts();
        System.out.println();
        bank.printAccounts(70.0,88.0);
        System.out.println();
        bank.displayAccount("Vlad",35.0);

    }

}
