package ujica.alexandru.lab6.e3;


public class BankAccount {
    public String owner;
    public double balance;

    public BankAccount(String owner, double balance) {
        this.owner=owner;
        this.balance=balance;
    }

    public double getBalance(){
        return this.balance;
    }

    public String getOwner() {
        return this.owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void withdraw(double amount) {
        this.balance -= amount;
    }

    public void deposit(double amount) {
        this.balance += amount;
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }


    public boolean equals(BankAccount o) {
        if (this == o) return true;
        return Double.compare(this.balance, o.balance) == 0 &&
                this.owner.equals(o.owner);
    }

    public int compareTo(Object o){
        BankAccount p = (BankAccount) o;
        if(balance>p.balance ) return 1;
        if(balance==p.balance) return 0;
        return -1;
    }

    public String toString(){
        return "("+owner+":"+balance+")";
    }
}

