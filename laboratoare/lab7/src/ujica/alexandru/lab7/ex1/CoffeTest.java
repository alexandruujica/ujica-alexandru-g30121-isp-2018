package ujica.alexandru.lab7.ex1;

public class CoffeTest {
    public static void main(String[] args) throws MyException {
        CofeeMaker mk = new CofeeMaker();
        CofeeDrinker d = new CofeeDrinker();
        for(int i = 0;i<15;i++) {
            try {
                if(i==10)throw new MyException("Too many!");
                Cofee c = mk.makeCofee();
                try {
                    d.drinkCofee(c);
                } catch (TemperatureException e) {
                    System.out.println("Exception:" + e.getMessage() + " temp=" + e.getTemp());
                } catch (ConcentrationException e) {
                    System.out.println("Exception:" + e.getMessage() + " conc=" + e.getConc());
                } finally {
                    System.out.println("Throw the cofee cup.\n");
                }
            } catch (IllegalArgumentException e) {
                System.out.println("Exception: Over 10 coffees ");
            }
        }
    }
}
