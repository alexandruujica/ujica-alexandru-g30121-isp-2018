package ujica.alexandru.lab7.ex4;

import java.io.*;
import java.util.*;

public class Car implements Serializable {
    String model;
    double price;

    public Car() {

    }

    public Car(String model, double price) {
        this.model = model;
        this.price = price;
    }

    public String getModel() {
        return model;
    }

    public double getPrice() {
        return price;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "model='" + model + '\'' +
                " have the price= " + price;
    }

    public static Car addElement() {
        Scanner scan = new Scanner(System.in);
        System.out.println("Model: ");
        String model = scan.nextLine();
        System.out.println("Price ");
        double price = scan.nextDouble();
        Car masina = new Car(model, price);
        return masina;
    }

    void save(String fileName) {
        try { ObjectOutputStream o =
                    new ObjectOutputStream(
                            new FileOutputStream(fileName));
            o.writeObject(this);
            System.out.println("Car saved in the folder");
        } catch (IOException e) {
            System.err.println("Car can't be saved");
            e.printStackTrace();
        }

    }
    static Car load(String fileName) throws IOException, ClassNotFoundException {
        ObjectInputStream in =
                new ObjectInputStream(
                        new FileInputStream(fileName));
        Car t = (Car)in.readObject();
        return t;
    }

}
