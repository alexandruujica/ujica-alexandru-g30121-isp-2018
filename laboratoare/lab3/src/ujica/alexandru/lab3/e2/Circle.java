package ujica.alexandru.lab3.e2;

public class Circle {
    private double radius = 1.0;
    private String color = "red";

    public void Circle() {
        this.radius = 1.0;
        this.color = "red";
    }

    public void Circle(double r, String c) {
        this.radius = r;
        this.color = c;
    }

    public void getRadius(double r) {
        this.radius = r;
    }

    public void getColor(String c) {
        this.color = c;
    }

    public void Screen() {
        System.out.println("The radius is " + this.radius + " and the color is " + this.color);
    }

}
