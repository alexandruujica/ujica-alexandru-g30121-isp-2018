package ujica.alexandru.lab3.e1;

public class Sensor {
    private int value;

    public Sensor() {
        value = -1;
    }

    public void change(int k) {
        this.value = k;
    }

    public int tString() {
        return this.value;
    }

}
