package ujica.alexandru.lab8.ex4;

public class HeatingUnit {
    private boolean o;

    public void setO(boolean o) {
        this.o = o;
    }

    public boolean isO() {
        return o;
    }
    public void heating(int desiredTemp)
    {
        System.out.println("Heating to reach desired temperature: "+desiredTemp);
    }
}
