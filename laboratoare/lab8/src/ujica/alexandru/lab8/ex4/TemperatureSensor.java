package ujica.alexandru.lab8.ex4;

import java.util.Random;

public class TemperatureSensor {
    private Random r=new Random();
    private int i;

    public Random getR() {
        return r;
    }

    public int getI() {
        i=r.nextInt();
        return i;
    }
}
