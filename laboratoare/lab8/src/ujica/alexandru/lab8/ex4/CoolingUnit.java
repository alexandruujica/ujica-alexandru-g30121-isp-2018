package ujica.alexandru.lab8.ex4;

public class CoolingUnit {
    private boolean n;

    public void setN(boolean n) {
        this.n = n;
    }

    public boolean isN() {
        return n;
    }
    public void cooling(int desiredTemp) {

        System.out.println("Cooling to reach desired temperature: " + desiredTemp);
    }
}
