package ujica.alexandru.lab8.ex4;


public class FireSensor {
    private boolean f;

    public boolean isF() {
        return f;
    }

    public void setF(boolean f) {
        this.f = f;
    }
}
