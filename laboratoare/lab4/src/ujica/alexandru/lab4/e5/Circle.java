package ujica.alexandru.lab4.e5;

public class Circle {
    double radius;
    private static final double pi = 3.14;
    String color;

    public Circle() {
        this.radius = 1.0;
        this.color = "red";
    }

    public Circle(double radius) {
        this.radius = radius;
        this.color = "red";
    }

    public double getRadius() {
        return radius;
    }

    public double getArea() {
        double a;
        a = Math.pow(this.radius, 2) * pi;
        return a;
    }
}
