package ujica.alexandru.lab4.e2;

public class TestAuthor {
    public static void main(String[] args) {
        Author a1 = new Author("Mircea Eliade", "mircea@gmail.com", 'm');
        System.out.println(a1.getName());
        System.out.println(a1.getEmail());
        System.out.println(a1.getGender());
        a1.setEmail("eliade@yahoo.com");
        System.out.println(a1.toString());
    }
}
