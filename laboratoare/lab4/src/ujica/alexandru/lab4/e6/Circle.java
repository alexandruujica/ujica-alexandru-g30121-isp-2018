package ujica.alexandru.lab4.e6;

public class Circle extends Shape {
    double radius=1.0;
    private static final double pi = 3.14;

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea() {
        double a;
        a = Math.pow(this.radius, 2) * pi;
        return a;
    }
    public String toString(){
        return "A Circle with radius= "+this.radius+" which is a subclass of "+this.toString();
    }
}

