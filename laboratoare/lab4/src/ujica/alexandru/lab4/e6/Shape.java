package ujica.alexandru.lab4.e6;

public class Shape {
    String color = "red";
    boolean filled = true;

    public Shape() {
    }

    public Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return this.filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    public String toString() {
        if (this.filled == true) {
            return "A Shape with color of " + this.color + " and filled";
        } else
            return "A Shape with color of " + this.color + " and Not filled";
    }
}
