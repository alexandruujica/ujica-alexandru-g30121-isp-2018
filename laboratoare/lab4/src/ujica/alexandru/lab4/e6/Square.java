package ujica.alexandru.lab4.e6;

public class Square extends Rectangle {
    public Square() {

    }

    public Square(double side) {
        this.width = Math.sqrt(side);
        this.length = this.width;
    }

    public Square(double side, String color, boolean filled) {
        this.width = Math.sqrt(side);
        this.length = this.width;
        this.color = color;
        this.filled = filled;
    }

    public double getSide() {
        return this.length * this.length;
    }

    public void setSide(double side) {
        this.length = Math.sqrt(side);
        this.width = this.length;
    }

    public void setLength(double side) {
        this.length = Math.sqrt(side);
        this.width = this.length;
    }

    public void setWidth(double side) {
        this.width = Math.sqrt(side);
        this.length = this.width;
    }

    public String toString() {
        return "A Square with side=" + this.getSide() + " ,which is a subclass of" + this.toString();
    }
}