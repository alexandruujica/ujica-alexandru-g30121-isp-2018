package ujica.alexandru.lab4.e1;

public class Circle {
    private double radius;
    private static final double pi= 3.14;
    private String color;
    public Circle(){
        this.radius=1.0;
        this.color="red";
    }
    public Circle(double radius){
        this.radius=radius;
        this.color="red";
    }

    public double getRadius() {
        return radius;
    }

    public double getArea(){
        double a;
        a=Math.pow(this.radius,2)*pi;
        return a;
    }
}
