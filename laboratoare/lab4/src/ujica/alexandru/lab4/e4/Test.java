package ujica.alexandru.lab4.e4;


public class Test {
    public static void main(String[] args) {
        int i;
        Author[] a1 = new Author[5];
        Author[] a2 = new Author[2];
        a1[0] = new Author("Vlad", "vlad@yahoo", 'm');
        a1[1] = new Author("Teo", "Teo@yahoo", 'm');
        a1[2] = new Author("Mihai", "Mihai@yahoo", 'm');
        a1[3] = new Author("Raluca", "ralu@yahoo", 'f');
        a1[4] = new Author("Sebi", "Sebi@yahoo", 'm');

        a2[0] = new Author("Vlad", "vlad@yahoo", 'm');
        a2[1] = new Author("Teo", "Teo@yahoo", 'm');
        Book b1 = new Book("Mihai", a1, 30.0);
        Book b2 = new Book("Padurea", a2, 32.0, 3);
        b1.setQtyInStock(5);
        System.out.println(b2.getQtyInStock());
        System.out.println(b1.getQtyInStock());
        for (i = 0; i < 5; i++) {
            System.out.println(b2.getAuthors());
        }
        b1.printAuthors();
    }
}