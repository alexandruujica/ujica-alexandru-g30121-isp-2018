package ujica.alexandru.lab4.e3;

public class Test {
    public static void main(String[] args) {
        Author a=new Author("Eliade","eliade@yahoo.com",'m');
        Book b1=new Book("Mihai",a,30.0);
        Book b2=new Book("Padurea",a,32.0,3);
        b1.setQtyInStock(5);
        System.out.println(b2.getQtyInStock());
        System.out.println(b1.getQtyInStock());
        System.out.println(b1.toString());
        System.out.println(b1.getPrice());
        System.out.println(b1.getAuthors());
    }
}
