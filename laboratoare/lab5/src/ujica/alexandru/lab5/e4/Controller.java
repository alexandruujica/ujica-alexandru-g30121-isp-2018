package ujica.alexandru.lab5.e4;

public class Controller {
    LightSensor l;
    TemperatureSensor t;

    private static Controller c1=new Controller();

    private Controller() {

    }
    public static Controller getInstance(){
        return c1;
    }

    public static void control() throws InterruptedException {
        int i;
        LightSensor a=new LightSensor();
        TemperatureSensor b=new TemperatureSensor();
            for (i = 0; i < 20; i++) {
                a.readValue();
                b.readValue();
                Thread.sleep(1000);
                System.out.println(a.getRand()+" "+b.getRand());
            }
        }
    }
