package ujica.alexandru.lab5.e4;

import ujica.alexandru.lab5.e3.Sensor;

import java.util.Random;

public class TemperatureSensor extends Sensor {

    Random rand=new Random();
    int a;
    @Override
    public int readValue() {
        a=rand.nextInt(100);
        return a;
    }

    public int getRand() {
        return a;
    }
}