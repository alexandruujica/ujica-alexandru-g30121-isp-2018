package ujica.alexandru.lab5.e3;

import java.util.Random;

public class LightSensor extends Sensor {
    Random rand=new Random();
    int a;
    @Override
    public int readValue()
    {
        a=rand.nextInt(100);
        return a;
    }

    public int getRand() {
        return a;
    }
}
