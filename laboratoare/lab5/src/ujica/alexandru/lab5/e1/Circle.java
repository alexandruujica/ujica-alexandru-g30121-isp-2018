package ujica.alexandru.lab5.e1;

public class Circle extends Shape {
    protected double radius;
    public Circle(){

    }
    public Circle(double radius){
        this.radius=radius;
    }

    public Circle(double radius, String color, boolean fileed) {
        super(color, fileed);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea(){
        return this.radius*this.radius*Math.PI;
    }

    @Override
    public double getPerimeter() {
        return 2*this.radius*Math.PI;
    }

    @Override
    public String toString() {
        return "Circle{}";
    }
}
