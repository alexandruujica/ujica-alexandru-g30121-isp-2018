package ujica.alexandru.lab5.e1;

public class Square extends Rectangle {
    public Square() {
    }

    public Square(double side) {
        this.width=Math.sqrt(side);
        this.length=this.width;
    }

    public Square(double side,String color, boolean fileed) {
        this.color=color;
        this.fileed=fileed;
        this.width=Math.sqrt(side);
        this.length=this.width;
    }
    public double getSide(){
        return this.width*this.width;
    }
    public void setSide(double side){
        this.width=Math.sqrt(side);
        this.length=this.width;
    }

    public void setWidth(double side) {
        super.setWidth(width);
    }


    public void setLength(double length) {
        super.setLength(length);
    }

    @Override
    public String toString() {
        return "Square{}";
    }

    @Override
    public double getArea() {
        return this.length*this.length;
    }

    @Override
    public double getPerimeter() {
        return 4*this.length;
    }
}

