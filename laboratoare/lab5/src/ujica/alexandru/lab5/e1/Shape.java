package ujica.alexandru.lab5.e1;

public abstract class Shape {
    protected String color;
    protected boolean fileed;

    public Shape(){
        this.color="red";
        this.fileed=true;
    }

    public Shape(String color, boolean fileed) {
        this.color = color;
        this.fileed = fileed;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFileed() {
        return fileed;
    }

    public void setFileed(boolean fileed) {
        this.fileed = fileed;
    }
    public abstract double getArea();
    public abstract double getPerimeter();

    @Override
    public String toString() {
        return "Shape{}";
    }
}

