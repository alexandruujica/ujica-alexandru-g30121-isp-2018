package ujica.alexandru.lab2.ex3;

import java.util.Scanner;

public class primes {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("The numbers:");
        int A = in.nextInt();
        int B = in.nextInt();
        int i, j = 0, k;
        boolean prime;
        System.out.println("Numerele prime intre "+A+" si "+B+" sunt:");
        for (k = A; k < B; k++) {
            prime = true;
            for (i = 2; i < k / 2; i++) {
                if (k % i == 0) {
                    prime = false;
                }
            }
            if (prime == true) {
                System.out.print(" " + k);
                j++;
            }
        }
        System.out.println(" ");
        System.out.println(" Nr de numere prime: " + j);
    }
}