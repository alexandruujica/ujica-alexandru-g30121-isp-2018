package ujica.alexandru.lab2.e6;

import java.util.Scanner;


public class FactorialNonrecursiv {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Numarul: ");
        int n = in.nextInt();
        int fact = 1;
        for (int i = 1; i <=n; i++) {
            fact = fact * i;
        }
        System.out.println("factorial: "+fact);
    }
}
