package ujica.alexandru.lab2.e6;

import java.util.Scanner;

class Calculate {
    int fact(int n) {
        int result;
        if (n == 1) {
            return 1;
        } else result = n * fact(n - 1);
        return result;
    }
}

public class FactorialRecursiv {

    public static void main(String[] args) {

        Calculate obj = new Calculate();
        Scanner in = new Scanner(System.in);
        System.out.print("Numarul:");
        int n = in.nextInt();
        int a = obj.fact(n);
        System.out.println("factorial: " + a);

    }
}
