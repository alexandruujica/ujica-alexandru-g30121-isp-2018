package ujica.alexandru.lab2.e5;

import java.util.Random;

public class Bublesort {
    public static void main(String[] args) {
        int a[] = new int[10];
        int aux;
        boolean verific = false;
        Random rand = new Random();
        for (int i = 0; i < 10; i++) {
            a[i] = rand.nextInt(40) + 1;
            System.out.print(a[i] + " ");
        }
        while (verific == false) {

            verific = true;
            for (int i = 1; i < 10; i++) {
                if (a[i - 1] > a[i]) {
                    aux = a[i];
                    a[i] = a[i - 1];
                    a[i - 1] = aux;
                    verific = false;
                }
            }
        }
        System.out.println("Sirul sortat:");
        for (int i = 0; i < 10; i++) {
            System.out.print(a[i] + " ");
        }

    }
}
