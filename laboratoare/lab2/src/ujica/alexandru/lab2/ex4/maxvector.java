package ujica.alexandru.lab2.ex4;

import java.util.Scanner;

public class maxvector {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int i, max;
        int a[] = new int[n];
        for (i = 0; i < n; i++) {
            a[i] = in.nextInt();
        }
        max = a[0];
        for (i = 1; i < n; i++) {
            if (max < a[i]) {
                max = a[i];
            }
        }
        System.out.print("maximul: " + max);

    }

}
