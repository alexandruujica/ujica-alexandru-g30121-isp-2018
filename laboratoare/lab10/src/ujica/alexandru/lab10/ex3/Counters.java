package ujica.alexandru.lab10.ex3;

public class Counters extends Thread {
    int a;
    Counters(String name,int s){
        super(name);
        this.a=s;
    }
    public void run(){
        for(int i=a;i<a+100;i++){
            System.out.println(getName()+" "+i);
            try{
                Thread.sleep((int)(Math.random()*100));
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        System.out.println(getName()+" job finalised.");
    }

    public static void main(String[] args) {
        Counters c1=new Counters("counter1",0);
        Counters c2=new Counters("counter2",100);
        c1.run();
        c2.run();
    }
}
