package ujica.alexandru.lab10.ex4;

public class robots {
    public static void main(String[] args) {
        Punct[] p = new Punct[10];
        Robot[] r = new Robot[10];
        for (int i = 0; i < 10; i++) {
            p[i] = new Punct();
            r[i] = new Robot(p[i],"Robotul nr."+i+" ");
        }

        for (int i = 0; i < 10; i++) {
            r[i].start();
        }
    }
}

class Robot extends Thread {
    Punct p;

    public Robot(Punct p,String name) {
        super(name);
        this.p = p;
    }

    public void run() {
        int i = 0;
        while (++i < 100) {
            int a = (int) Math.round(10 * Math.random() + 10);
            int b = (int) Math.round(10 * Math.random() + 10);
            try {
                sleep(150);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println(getName()+"e aici: " + a + " ; " + b);
        }

    }
}

class Punct {
    int x, y;

    public void setXY(int a, int b) {
        x = a;
        y = b;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }
}
